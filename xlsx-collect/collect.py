from fire import Fire
from pprint import pprint
import enchant
import json
import os
import re
import xlwt
import xlrd
from tqdm import tqdm
import numpy as np
from numpy.linalg import norm

from utils import read_xls, write_sheet


def populate_tags(field):
    dictionary = enchant.Dict("ru_RU")
    allowed_tokens = {
        "к-кт",
        "гидрокомпенсатор",
        "рем.к-т",
        "Выключатель",
        "стеклолодъемник",
    }
    tags = field.split(" ")

    def check(tag):
        return len(tag) > 2 and (dictionary.check(tag) or tag in allowed_tokens)

    return sorted([tag.lower() for tag in tags if check(tag)])


def clean(field):
    field = field.replace("!", " ")
    field = field.replace("/", " ")
    field = field.replace("_", " ")
    field = re.sub(r"[+-]?([0-9]*[.])?[0-9]+", "", field)
    return field


def tokenize(rows, fieldname_to_tag):
    tokens = []
    for idx, row in enumerate(tqdm(rows, desc="tokenize  ")):
        cleaned = clean(row[fieldname_to_tag])
        tokens.append(populate_tags(cleaned))
    return tokens


def build_idf(tokens, index_maxlength=10 * 1000):
    index = {}
    for token_list in tqdm(tokens, desc="build idf "):
        for token in token_list:
            if token not in index:
                index[token] = 0
            index[token] += 1
    # if len(index) > index_maxlength:
    #     idf = sorted(index, key=index.get, reverse=True)[:index_maxlength]
    # else:
    #     idf = list(index.keys())
    idf = list(index.keys())
    return idf


def collect_groups(vectors, equal):
    groups = []
    for idx, vec in enumerate(tqdm(vectors, desc="clusterize")):
        found = False
        for group_idx, group in enumerate(groups):
            for item_idx in group:
                item_vec = vectors[item_idx]
                if equal(vec, item_vec):
                    groups[group_idx].append(idx)
                    found = True
                    break
        if not found:
            groups.append([idx])
    return groups


def collect(
    xls_path,
    xls_sheet=0,
    theta=0.98,
    fn_tag="описание",
    fn_number="кол-во",
    fn_sum="сумма",
    index_maxlength=1000 * 1000,
    fn_result_inter_group_id="group_id",
    fn_result_inter_self_id="id",
    fn_result_inter_token="tokens",
    fn_result_tokens="Tokens",
    fn_result_numbers="Numbers",
    fn_result_sums="Sums",
    output_path="./out",
):
    assert fn_result_tokens != fn_result_numbers
    assert fn_result_tokens != fn_result_sums
    assert fn_result_numbers != fn_result_sums
    assert index_maxlength > 1
    assert 0 < theta <= 1
    assert os.path.isdir(output_path)

    rows, headers = read_xls(xls_path, xls_sheet=xls_sheet)
    assert fn_result_inter_group_id not in headers
    assert fn_result_inter_self_id not in headers

    tokens_json_path = os.path.join(output_path, "tokens.json")
    if os.path.exists(tokens_json_path):
        tokens = json.load(open(tokens_json_path, "r"))
        print(f"tokens <- {tokens_json_path}")
    else:
        tokens = tokenize(rows, fieldname_to_tag=fn_tag)
        json.dump(tokens, open(tokens_json_path, "w"))
        print(f"tokens -> {tokens_json_path}")

    idf = build_idf(tokens, index_maxlength=index_maxlength)

    vectors = []
    for token_list in tqdm(tokens, desc="vectorize "):
        vectors.append([id in token_list for id in idf])

    vectors = np.array(vectors).astype("int")

    def equal(vec1, vec2):
        cosd = np.dot(vec1, vec2) / norm(vec1) / norm(vec2)
        return cosd >= theta

    groups = collect_groups(vectors, equal=equal)

    # write results
    workbook = xlwt.Workbook()

    numbers = []
    sums = []
    tokens_by_group = []
    for group in groups:
        numbers.append(0.0)
        sums.append(0.0)
        tokens_by_group.append([])
        for idx in group:
            numbers[-1] += rows[idx][fn_number]
            sums[-1] += rows[idx][fn_sum]
            tokens_by_group[-1] += tokens[idx]
        tokens_by_group[-1] = np.unique(tokens_by_group[-1])
        if tokens_by_group[-1] == []:
            tokens_by_group[-1] = [rows[idx][fn_tag] for idx in group]

    result_headers = [fn_result_tokens, fn_result_numbers, fn_result_sums]
    result_rows = []
    for group_id, (token_list, number, sm) in enumerate(
        zip(tokens_by_group, numbers, sums)
    ):
        if token_list != []:
            rtokens = " ".join(token_list)
        else:
            rtokens = "(!) " + " ".join([rows[idx][fn_tag] for idx in groups[group_id]])
        result_rows.append(
            {
                fn_result_tokens: rtokens,
                fn_result_numbers: number,
                fn_result_sums: sm,
                "group id": group_id,
                "group size": len(groups[group_id])
            }
        )
    result_rows = sorted(result_rows, key=lambda x: x[fn_result_tokens])

    output_xlsx_path = os.path.join(output_path, "results.xlsx")
    write_sheet(
        rows=result_rows,
        headers=result_headers + ["group id", "group size"],
        output_path=output_xlsx_path,
        wb=workbook,
        sheet_name="results_final",
    )

    for group_idx, group in enumerate(groups):
        for idx in group:
            rows[idx][fn_result_inter_self_id] = idx
            rows[idx][fn_result_inter_group_id] = group_idx
            rows[idx][fn_result_inter_token] = tokens[idx]

    write_sheet(
        rows=rows,
        headers=headers
        + [fn_result_inter_self_id, fn_result_inter_group_id, fn_result_inter_token],
        output_path=output_xlsx_path,
        wb=workbook,
        sheet_name="results_inter",
    )
    print(f"{len(result_rows)} rows -> {output_xlsx_path}")


if __name__ == "__main__":
    Fire(collect)
